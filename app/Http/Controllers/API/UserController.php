<?php

namespace App\Http\Controllers\API;

use App\Actions\Fortify\PasswordValidationRules;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Exception;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Helpers\ResponseFormatter;

class UserController extends Controller
{
    use PasswordValidationRules;

    public function login(Request $request){
        try{
            $request->validate([
                'email' => 'email|required',
                'password' => 'required'
            ]);
            $credentials = request(['email', 'password']);
            if(!Auth::attempt($credentials)){
                return ResponseFormatter::error([
                    'message'   =>  'Unauthorized'
                ], 'Authentication Failed',500);
            }
            $user = User::where('email', $request->email)->first();
            if(!Hash::check($request->password, $user->password, [])){
                throw new \Exception('Invalid Credentials');
            }
            $tokenResult = $user->createToken('authToken')->plainTextToken;
            return ResponseFormatter::success([
                'access_token'  => $tokenResult,
                'token_type'    => 'Barier',
                'user'          => $user
            ], 'Authenticated');

        }catch(Exception $error){
            return ResponseFormatter::error([
                'message'   =>  'Unauthorized',
                'error'     => $error
                ], 'Authentication Failed',500);
        }
    }

    public function register(Request $request){
        try{
            $request->validate([
                'name'  => ['required', 'string', 'max:255'] ,
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'] ,
            ]);
            User::create([
                'name'         => $request->name,
                'email'        => $request->email,
                'address'      => $request->address,
                'houseNumber'  => $request->houseNumber,
                'phoneNumber'  => $request->phoneNumber,
                'city'         => $request->city,
                'password'     => Hash::make($request->password)
            ]);

            $user   =   User::where('email', $request->email)->first();
            $tokenResult    =  $user->createToken('authToken')->plainTextToken;
            return ResponseFormatter::success([
                'access_token'  =>  $tokenResult,
                'token_type'    =>  'Bearer',
                'user'          => $user
            ]);
            
        }catch(Exception $error){
            return ResponseFormatter::error([
                'message'   =>  'Unauthorized',
                'error'     => $error
                ], 'Authentication Failed',500);
        }
    }
    public function deleteUser($id){
        try{
            $user = User::find($id);
            $user->delete();
            return ResponseFormatter::success('Status', true);
        }catch(Exception $error){
            return ResponseFormatter::error([
                'message'   =>  'Unauthorized',
                'error'     => $error
                ], 'Authentication Failed',500);
        }
        
    }
    public function logout(Request $request){
        $token  = $request->user()->currentAccessToken()->delete();
        return ResponseFormatter::secces($token, 'toekn Revoked');
    }
    public function fetch(Request $request){
        return ResponseFormatter::success(
            $request->user(), 'Data Profile user berhasil diambil'
        );
    }
    public function updateProfile(Request $request){
        $data = $request->all();

        $user = Auth::user();
        $user->update($data);

        return ResponseFormatter::success($user, 'Profile Updated');
    }
    public function updatePhoto(Request $request){
        // $validator = Validaor::make($request->all(),[
        //     'file'  =>  'required|image|max:2848'
        // ]);
        // if($validator->fails()){
        //     return ResponseFormatter::error([
        //         'error' => $validator->errors()
        //     ], 'Update photo fails', 401);
        // }
        
        if($request->file('file')){
            $file = $request->file->store('assets/user','public');
            //simpan foto ke db(urlya)
            $user = Auth::user();
            $user->profile_photo_path   =   $file;
            $user->update();
            return ResponseFormatter::success([$file], 'File successful upload');

        }
    }

}
