<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Food;
use App\Helpers\ResponseFormatter;

class FoodController extends Controller
{
    public function create(Request $request){
        try{
            $request->validate([
                'user_id'  => ['required'] ,
                'name' => ['required', 'string','max:255'],
                'price' => ['required', 'string'] ,
            ]);
            User::create([
                'user_id'      => $request->user_id,
                'name'         => $request->name,
                'description'  => $request->description,
                'ingredients'  => $request->ingredients,
                'price'  => $request->price,
                'rate'         => $request->rate,
                'types'     => $request->types,
                'picturePath'     => $request->picturePath,

            ]);

            return ResponseFormatter::success([
                'food'          => "Data Berhasil Ditambahkan"
            ]);
            
        }catch(Exception $error){
            return ResponseFormatter::error([
                'message'   =>  'Unauthorized',
                'error'     => $error
                ], 'Authentication Failed',500);
        }
    }
    public function deleteFood($id){
        try{
            $user = Food::find($id);
            $user->delete();
            return ResponseFormatter::success('Status', true);
        }catch(Exception $error){
            return ResponseFormatter::error([
                'message'   =>  'Unauthorized',
                'error'     => $error
                ], 'Authentication Failed',500);
        }  
    }
    public function updateFood(Request $request, $id){
        $data = $request->all();

        $food=Food::find($id);
        $food->update($food);

        return ResponseFormatter::success($food, 'Food Updated');
    }
    public function all(Request $request){
        $name = $request->input('name');
        $limit = $request->input('limit',6);
        $types = $request->input('types');
        
        $price_from = $request->input('price_from');
        $price_to = $request->input('price_to');

        $rate_from = $request->input('rate_from');
        $rate_to = $request->input('rate_to');

        $food = Food::query();
        if($name){
            $food->where('name','like','%'.$name.'%');
        }
        if($types){
            $food->where('types','like','%'.$types.'%');
        }
        if($price_from){
            $food->where('price','>=',$price_from);
        }
        if($price_to){
            $food->where('price','<=',$price_from);
        }
        if($rate_from){
            $food->where('rate','>=',$rate_from);
        }
        if($rate_to){
            $food->where('rate','<=',$rate_to);
        }

        return ResponseFormatter::success(
            $food->paginate($limit),
            'Data List Product berhasil ditampilkan'
        );
    }
}
