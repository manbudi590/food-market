<?php

namespace App\Http\Controllers\API;

use App\Models\Food;
use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Transaction;
use App\Helpers\ResponseFormatter;
use Illuminate\Support\Facades\Auth;


class TransactionController extends Controller
{
    public function all(Request $request){
        $id = $request->input('id');
        $status = $request->input('status');
        $limit = $request->input('limit',6);
        $food_id = $request->input('food_id');
        
        if($id){
            $transaction= Transaction::with(['food', 'user'])->find($id);
            if($transaction){
                return ResponseFormatter::success(
                    $transaction,
                    'Data Transaksi berhasil di input',
                );
            
            }else{
                return ResponseFormatter::error(
                    null,
                    'Data Transaksi gagal diinput',
                    404
                );
            }
        }
        $transaction = Transaction::with(['user', 'food'])
                        ->where('user_id', Auth::user()->id);
        if($food_id){
            $transaction->where('food_id', $food_id);
        }
        if($status){
            $transaction->where('status',$status);
        }
        return ResponseFormatter::success(
            $transaction->paginate($limit),
            'Data List Transaksi berhasil ditampilkan'
        );
    }
    public function update(Request $request, $id){
        $transaction = Transaction::findOrFail($id);
        $transaction->update($request->all());
        return ResponseFormatter::success($transaction, 'Transaksi berhasil diperbarui');
    }
}
